import React, { Component } from 'react';
import contentimage from '../images/whitecard-p.PNG';
import '../App.css';
import '../css/sizeselection.css';
import contentimage2 from '../images/whitecard-l.PNG';
import { Link } from "react-router-dom";
import {connect} from 'react-redux';
import * as actions from '../Actions/sizeselection-actions';
import SubHeaderWizard from './SubHeaderWizard';

class SizeSelection extends Component {

updateSize(id){
    if(id!==this.props.size){
         this.props.updateSize(id);
    }
}

getStyle(id) {

if(id===this.props.size){
    return{
      border :'4px solid #2DCD53'
    }
}
}


 render() {

     
    let currentloc='';
     if(this.props.match.isExact){
         currentloc=this.props.location.pathname;
        console.log(this.props.match);
     }
    
    
    return (
    
      <div className="App">
        <div className="container content">
        <SubHeaderWizard currentloc={currentloc}/>
            
            <div className=" header text-center ">
                <h2><strong>ORIENTATION</strong></h2>
                
             </div>
           

              <div className="row">
                    <div className="col-md-4">
                        <div className="left-sidebar">
                            <h2><strong>Selected Size</strong></h2>
                            <p><span id="SizeofImage" className="orientation">{this.props.size}</span>in</p>
                            <p> <strong> Card orientation </strong></p>
                          </div>


                      </div>
                    
                    <div className="col-md-8" >
                          <div className="row">
                                 <div className="col-md-1">
                                
                                  </div>
                                <div className="col-md-3 right-sidebar">
                                {/* <router-link to="/SizeSelection/1">
                                onClick={this.updateSize.bind(this,"5x7")} */}
                                    <img className="selection-image1" style = {this.getStyle("5x7")}  alt="no" src={contentimage} />
                                      <p><strong> Portrait</strong></p>
                                 {/* </router-link> */}
                                </div>
                                  {/* <div className="col-md-1 mid-sidebar">
                                      <p>or </p>
                                  </div>
                                  <div className="col-md-4 right-sidebar" >
                                  <router-link to="/2">
                                    <img onClick={this.updateSize.bind(this,"3x6")} className="selection-image2" style = {this.getStyle("3x6")} alt="no" src={contentimage2}   />
                                   </router-link>
                                   <p><strong> Landscape</strong></p>
                                  </div> */}

                              </div>
                    </div>
                  </div>

                

                  <div className="row">
                <div className="col-md-6">
                        <div className="pull-right">
                        <Link to="/" className="btn btn-warning contentButton" >Go Back</Link>


                        </div>
                </div>
                <div className="col-md-6 ">
                        <div className="pull-left">
                        <Link to="/Customize/" className="btn btn-warning contentButton">Create Card</Link>
                        </div>
                    </div>
                </div>
              </div>
         </div>
    );
  }
}

function mapStateToProps(state){
    let st=state.sizeselection;
    return {
        size:st.size
    }
}
export default connect(mapStateToProps,actions)(SizeSelection);
