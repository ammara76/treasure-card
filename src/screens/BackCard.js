import React, { Component } from 'react';
import '../App.css';
import '../css/customize.css';
import { Button,ButtonGroup,FormGroup, } from 'reactstrap';
import { Link } from "react-router-dom";
import barcodepink from '../images/barcodepink.png';
import barcode from '../images/barcos.png';
import {connect} from 'react-redux';
import * as actions from '../Actions/customize-actions';


class BackCard extends Component {
constructor(props) {
    super(props);

   console.log('all props',this.props);
}

render() {
     return (
     
       <div>
      <p className="lower-backcard-heading"><i>See the rest of the story by scanning this QRCode Below </i></p>

      <div className="lower-backcard-imagewrapper">
          <img className="align-middle lower-backcard-image" src={barcode} alt="no" />
      </div>

      <p className="lower-backcard-text"><i>Treasured</i> </p>
         
       </div>

    )
}

}

function mapStateToProps(state){
  
  return {
    customize:state.customize
  }
}
export default connect(mapStateToProps,actions)(BackCard);