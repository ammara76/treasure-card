import {  PAYMENTINPUTCHANGE } from './actions-type';

export function handleInputChange(name,value){
    return{
        type: PAYMENTINPUTCHANGE,
        name:name,
        value:value
    }
};
