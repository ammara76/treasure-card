import React, { Component } from 'react';
import '../App.css';
import '../css/thankyou.css';
import icon from '../images/thankyou-icon.PNG';
import { Link } from "react-router-dom";
import SubHeaderWizard from './SubHeaderWizard';
class Thankyou extends Component {

 render() {
    let currentloc='';
     if(this.props.match.isExact){
         currentloc=this.props.location.pathname;
        //console.log(this.props.match);
     }
    
    
    return (
    
      <div className="App">
        <div className="container content">
        <SubHeaderWizard currentloc={currentloc}/>
        
              <div className="thankyou-content">
                  <h2><strong>CONFIRMATION</strong></h2>
                  <p> <strong>#06439479</strong> </p>
                    <img src={icon} alt="no" height='70px' width="70px"/>

                    <h1><strong>THANKYOU</strong></h1>
              </div>

              <div className="row text-center">

                  <Link className="btn btn-warning contentButton" to="/SizeSelection/">Create Another Treasure Card</Link>


        </div>
         </div>
         </div>
    );
  }
}

export default Thankyou;
