import React, { Component } from 'react';
import '../App.css';
import '../css/customize.css';
import { Button,ButtonGroup,FormGroup, } from 'reactstrap';
import { Link } from "react-router-dom";


class InnerLeftCard extends Component {
constructor(props) {
    super(props);
  
}


render() {
     return (
     
       <div>
                <div className=" row" >
                    <div className="col-lg-6 col-md-6 col-sm-6 lower-middlecard-imagewrapper1 " style={this.props.getImageStyle} >
                    {this.props.middlePreviewimage1}
                        
                    </div>
                    <div   className="col-lg-6 col-md-6 col-sm-6 lower-middlecard-imagewrapper2 ">
                    {this.props.middlePreviewimage2}
                        
                    </div>
                                     
                </div>
       </div>

    )
}

}
export default InnerLeftCard;
