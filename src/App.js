import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";
import SizeSelection from './screens/SizeSelection';
import Header from './screens/Header';
import Footer from './screens/Footer';
import Media from './screens/Media';
import SignUp from './screens/SignUp';
import FAQ from './screens/FAQ';
import Home from './screens/Home';
import Customize from './screens/Customize';
import Delivery from './screens/Delivery';
import Payment from './screens/Payment';
import Thankyou from './screens/Thankyou';




class App extends Component {

  render() {


    return (
      <Router>
      <div>

        
              <Header />
              

              <div className="container">
              <div  >
                <Route exact path="/" component={Home} />
                <Route exact path="/Media" component={Media} />
                <Route exact path="/FAQ" component={FAQ} />
                <Route exact path="/SignUp" component={SignUp} />
                <Route exact path="/SizeSelection" component={SizeSelection} />
                <Route exact path="/Customize" component={Customize} />
                <Route exact path="/Delivery" component={Delivery} />
                <Route exact path="/Payment" component={Payment} />
                <Route exact path="/Thankyou" component={Thankyou} />
              </div>

              

            

              </div>
              <Footer />
    </div>

    </Router>

    );
  }
}


export default App;
