import React, { Component } from 'react';
import '../App.css';
import '../css/payment.css';
import machine from '../images/machine.png';
import { Link } from "react-router-dom";
import {connect} from 'react-redux';
import * as actions from '../Actions/payment-actions';
import SubHeaderWizard from './SubHeaderWizard';
import InjectedCheckoutForm from './CheckoutForm';
//import InjectedPaymentRequestForm from './PaymentRequestButton';
import {Elements} from  'react-stripe-elements';
import {StripeProvider} from 'react-stripe-elements';
import { Form, FormGroup, Label,Button, Input, FormFeedback, FormText,Row,Col } from 'reactstrap';
import { ValidatingFormGroup } from 'reactstrap-validation';
import { AvForm, AvField, AvGroup, AvInput, AvFeedback, AvRadioGroup, AvRadio } from 'availity-reactstrap-validation';

class Payment extends Component {

    constructor(props) {
        super(props);
        this.state = 
        {
          stripe: null
        };

       this.handleInputChange = this.handleInputChange.bind(this);
}

componentDidMount() {

    if (window.Stripe) {
      this.setState({stripe: window.Stripe('pk_test_DfeY2XIWkIKC9MkzeLjjzx24')});
    
    } else {
            document.querySelector('#stripe-js').addEventListener('load', () => {
            // Create Stripe instance once Stripe.js loads

            this.setState({stripe: window.Stripe('pk_test_DfeY2XIWkIKC9MkzeLjjzx24')});
      });
    }
  }
    
handleInputChange(event) {
    const target = event.target;
    const value =  target.value;
    const name = target.name;

    this.props.handleInputChange(name,value);

  }

 render() {

    let currentloc='';
     if(this.props.match.isExact){
         currentloc=this.props.location.pathname;
        //console.log(this.props.match);
     }
    
    
    return (
    
      <div className="App">
        <div className="container content">
        <SubHeaderWizard currentloc={currentloc}/>
        
           <div className="customize-header">
             <h3> <strong>BILLING</strong> </h3>
             <img src={machine} alt="no" height='70px' width="70px"/>
           </div>
                    {/* <Row>

                        <Col sm={4}>
                        <AvForm>
                                
                                <AvField name="name" type="email" label="Name" required />
                            
                                <AvGroup>
                                <Label for="example">Rank</Label>
                                <AvInput name="rank" type="number" id="example" required />
                                <AvFeedback>This is an error!</AvFeedback>
                                </AvGroup>
                              
                                
                                <FormGroup>
                                <Button>Submit</Button>
                                </FormGroup>
                            </AvForm>
                        </Col>
                        </Row> */}
                        

           <div className="row">
                <div className="col-md-4">

                    <div className="payment-rightbar">
                        <h2> <strong>Total</strong> </h2>
                        <h2 className="price"><strong>${this.props.price}.00</strong> </h2>
                    </div>

                </div>

                <div className="col-md-8">

                    <div className="row">
                        <form className="form-inline left-sidebar-form" >
                                    <div className="form-group">
                                        <label htmlFor="First ">First Name:</label>
                                        <input onChange={this.handleInputChange}  value={this.props.firstName}  name="firstName" ref="firstName"  type="text " className="form-control"/>
                                    </div>
                
                                    <div className="form-group">
                                        <label htmlFor="Last">Last Name:</label>
                                        <input  onChange={this.handleInputChange}  value={this.props.lastName} name="lastName" ref="lastName" type="text" className="form-control"/>
                                    </div>
                        
                        </form>

                       
                    </div>


                    <div className="row">
                            <div className="col-md-6 checkoutform">
                                <StripeProvider stripe={this.state.stripe}>

                                    <Elements>
                                        <InjectedCheckoutForm />
                                        {/* <InjectedPaymentRequestForm /> */}
                                    </Elements>
                                </StripeProvider>

                            </div>
                    </div>
            </div>

        </div>

            <div className="row">
            <div className="col-md-6">
                    <div className="pull-right">
                    <Link className="btn btn-warning contentButton" to="/Delivery/">Go Back</Link>


                    </div>
            </div>
            <div className="col-md-6 ">
                    <div className="pull-left">
                    <Link className="btn btn-warning contentButton" to="/Thankyou"> Finalize</Link>

                    </div>
                </div>
            </div>

    
        </div>
     </div>

     
    );
  }
}

function mapStateToProps(state){

    let customize=state.customize;
    let payment=state.payment;
    return {
        price:customize.price,
        firstName:payment.firstName,
        lastName:payment.lastName,
        cardNumber: payment.cardNumber,
        cardExpiry:payment.cardExpiry,
        cardCVC:payment.cardCVC,
        couponCode:payment.couponCode
    }
  }


  export default connect(mapStateToProps,actions)(Payment);


  //not Required

       {/* <Form>
                                <ValidatingFormGroup trigger="change" valid={true} onStateChange={console.log}>
                                    <Input required type="email" />
                                    <FormFeedback>Please Enter the required field!</FormFeedback>
                                </ValidatingFormGroup>
                            </Form>

                            <Form>
                                <ValidatingFormGroup trigger="change" valid>
                                <Label for="exampleEmail">Valid input</Label>
                                <Input valid />
                                <FormFeedback valid>Sweet! that name is available</FormFeedback>
                                <FormText>Example help text that remains unchanged.</FormText>
                                </ValidatingFormGroup>
                                <FormGroup>
                                <Label for="examplePassword">Invalid input</Label>
                                <Input invalid />
                                <FormFeedback>Oh noes! that name is already taken</FormFeedback>
                                <FormText>Example help text that remains unchanged.</FormText>
                                </FormGroup>
                        </Form> */}