import React, { Component } from 'react';
import '../App.css';
import '../css/customize.css';
import { Button,ButtonGroup,FormGroup, } from 'reactstrap';
import { Link } from "react-router-dom";
import contentimage2 from '../images/whitecard-l.PNG';
class InnerRightCard extends Component {
constructor(props) {
    super(props);
  
}


render() {
     return (
     
       <div>
            <p className="lower-middlecard-subTitle" >{this.props.insidecardTitle}</p>
                  <img className="align-middle middlecard-icon" src={contentimage2} alt="no" height='24px' width='24px'/>
                  <p className="lower-middlecard-date text-muted ">{this.props.insidecardDate}</p>
                  
                  <div className="lower-middlecard-description">
                    <p className="description"><i>{this.props.insidecardDesc}</i></p>
                  </div>
                  <div className="lower-middlecard-signature">
                  <p className="subTitle" >{this.props.insidecardSignature}</p>
                    </div>
       </div>

    )
}

}
export default InnerRightCard;
