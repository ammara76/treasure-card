import { SUBMITDELIVERYFORM, DELIVERYINPUTCHANGE } from './actions-type';


export function submitDeliveryForm(data){
    return{
        type:SUBMITDELIVERYFORM,
        data:data
    }
};



export function handleInputChange(name,value){
    return{
        type: DELIVERYINPUTCHANGE,
        name:name,
        value:value
    }
};
