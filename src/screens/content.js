import React, { Component } from 'react';
import contentimage from '../images/whitecard.jpg';
import '../App.css';
import { Link } from "react-router-dom";
import { Button } from 'reactstrap';

class Content extends Component {
  render() {
 //console.log( this.props.location.pathname);
     
 let currentloc=this.props.location.pathname;

 

    return (
      <div className="App">
        <div className="container content">

       
              <div >
                  <img className="content-image" src={contentimage} alt="no" width="100" height="50" />
              </div>

              <div className="row text-center">

                  <Button className="contentButton">
                        <Link className="nav-items" to="/SizeSelection">Start</Link></Button>{' '}
                  
              </div>
          </div>
      </div>
    );
  }
}

export default Content;
