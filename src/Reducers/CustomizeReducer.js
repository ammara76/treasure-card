import {INCREMENT, DECREMENT,CHECKBOX, INSIDECARDIMAGE2CHANGE,INSIDECARDIMAGE1CHANGE, FRONTCARDIMAGECHANGE,
         FORMSUBMIT ,CUSTOMIZEINPUTCHANGE} from '../Actions/actions-type';
var moment = require('moment');
const initialState = {
        price:15,
        incrementValue:15,
        checked:true,
        numberOfCards:1,
        record: false,
        recordedFileUrl:'',
        frontcardTitle:'LOVE',
        frontcardSubTitle:'FOREVER',
        frontcardDesc:'Dio ti Benedica Maria',
        insidecardTitle:'Obrey Graham is Born',
        insidecardDate:'20 Oct,2017',
        insidecardSignature:'Signature',
        insidecardDesc:'Description',
        frontcardFile: '',
        frontcardPreviewUrl:'',
        insidecardFile1: '',
        insidecardFile2:'',
        insidecardPreviewUrl1:'',
        insidecardPreviewUrl2:''
    
};

export default function (state=initialState,action){

    switch(action.type){
            case INCREMENT:
                return {
                    ...state,
                    numberOfCards:state.numberOfCards+1,
                    price : state.price + action.incrementValue
                };
            break;

            case DECREMENT :
                return {
                    ...state,
                    numberOfCards:state.numberOfCards-1,
                    price : state.price - action.incrementValue
                };
                
            break;

            case CHECKBOX :
                return {
                    ...state,
                    price:action.price,
                    // numberOfCards:1,
                    incrementValue:action.incrementValue,
                    checked:!state.checked
                }
            break;
            

            case FORMSUBMIT:
               
            
                return{...state,
                    frontcardTitle:action.data.frontcardTitle,
                    frontcardSubTitle:action.data.frontcardSubTitle,
                    frontcardDesc:action.data.frontcardDesc,
                    }
            
    
            break;
            
            case CUSTOMIZEINPUTCHANGE:
            return {
                ...state,
               [action.name] : action.value
            };
            break;

            case 'STOP':
            return {
                ...state,
                record:false,
            };
            break;

            case 'START':
            return {
                ...state,
               record:true,
            };
            break;

            case FRONTCARDIMAGECHANGE:
            
            return {
                ...state,
                frontcardFile: action.file,
                frontcardPreviewUrl: action.previewUrl
            };
            break; 

            case INSIDECARDIMAGE1CHANGE:
          
            return {
                ...state,
                insidecardFile1: action.file,
                insidecardPreviewUrl1: action.previewUrl
            };
            break;

            case INSIDECARDIMAGE2CHANGE:
            console.log(state);
            return {
                ...state,
                insidecardFile1: action.file,
                insidecardPreviewUrl2: action.previewUrl
            };
            break;

            case 'SAVERECORDING':
            return {
                ...state,
                recordedFileUrl:action.recordedFileUrl
            }
            break;

            case 'DATECHANGE':
            return {
                ...state,
                insideCardDate:action.date
            }
            break;
            
                default:
                    return state;
            }
}