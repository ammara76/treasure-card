import { SUBMITDELIVERYFORM  ,PAYMENTINPUTCHANGE} from '../Actions/actions-type';

const initialState = {
    firstName:'',
    lastName:'',
    cardNumber: '',
    cardExpiry:'',
    cardCVC:'',
    couponCode:''
};

export default function (state=initialState,action){
    switch(action.type){
        case PAYMENTINPUTCHANGE:
            return {
                ...state,
                [action.name] : action.value
            };
            break;

    
            default:
                return state;
        }
}