import React, { Component } from 'react';
import '../App.css';
import '../css/customize.css';
import { Button,ButtonGroup,FormGroup, } from 'reactstrap';
import { Link } from "react-router-dom";



class FrontCard extends Component {
constructor(props) {
    super(props);


}




render() {
     return (
     
       <div>
                <div className="inputcard">
                          <h1 className="title">{this.props.frontcardTitle.toUpperCase()}</h1>
                          <p className="subTitleFrontCard">{this.props.frontcardSubTitle}</p>
                          <div className="lower-frontcard-description">
                              <p >{this.props.frontcardDesc}</p>
                          </div>
                        
                             <div className="lower-frontcard-imagewrapper">
                                  {this.props.frontPreviewimage}
                             </div>

                        </div>
       </div>

    )
}

}
export default FrontCard;