import { FRONTCARDIMAGECHANGE } from "./d:/Projects/treasure-card/src/Actions/actions-type";

-made all tiles capitalized
-added ORIENTATION title to SIZE SELECTION screen
-changed SELECT to Select
 
**Story Card Customize**
-Adaptive price 
-Removing Sound module should not remove inside
-Bigger Description Box
-Updated Inside Card: left (image1,image2), right(title, description)
-**For Sound Module:** Sound icon above Audio File, Record Icon above Record button
-Add choose file for sound module
-added Record button
-Removed : "Or (Phone Image)"
-added: date picker (calendar opens up)
-Added Thank you / Signature field in backcard
-Short Description in frontCard 
-changed "Paper Quality" to 'Card Quantity' 
-Done: price is multiplied by number of cards: Each card is 10$, 15$ with sound module  for each card )

**On delivery**
-Removed Grey "Submit" Button
-Added "Email" Field

**On Payment **
-added Stripe UI 

**Finalize Page** 
-replaced "Go Back" button with "Create another TreasureCard"