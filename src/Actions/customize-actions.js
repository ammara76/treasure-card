import {INCREMENT, DECREMENT, CHECKBOX, FORMSUBMIT, CUSTOMIZEINPUTCHANGE,
    INSIDECARDIMAGE2CHANGE,INSIDECARDIMAGE1CHANGE, FRONTCARDIMAGECHANGE} from './actions-type';


export function increment(incrementValue){
    return{
        incrementValue:incrementValue,
        type: INCREMENT
    }
};


export function decrement(incrementValue){
    return{
        incrementValue:incrementValue,
        type: DECREMENT
    }
};


export function handleCheckbox(incrementValue,price){
    return{
        price:price,
        incrementValue:incrementValue,
        type: CHECKBOX
    }
};

export function handleImageChange(imageid,file,url){

    let imageurl=null;

    if(imageid===1){
        imageurl=FRONTCARDIMAGECHANGE;    
        
    }if(imageid===2){
        imageurl=INSIDECARDIMAGE1CHANGE;
    }if(imageid===3){
        imageurl=INSIDECARDIMAGE2CHANGE;
    }

    return {
        type: imageurl,
        file:file,
        previewUrl:url
    }
   
}

export function submitForm(mydata){
    return {
        type:FORMSUBMIT,
        data:mydata
    }
}


export function stopRecording(){
    return {
        type:'STOP',
    }
}

export function startRecording(){
    return {
        type:'START',
    }
}

export function setRecordedFile(recordedFileUrl){
    return {
        type:'SAVERECORDING',
        recordedFileUrl:recordedFileUrl

    }
}

export function handleInputChange(name,value){
    return{
        type: CUSTOMIZEINPUTCHANGE,
        name:name,
        value:value
    }
};

export function handleDateChange(date){
    return {
        type:'DATECHANGE',
        date:date,
    }
}