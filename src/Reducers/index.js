import { combineReducers } from 'redux';
import CustomizeReducer from './CustomizeReducer';
import DeliveryReducer from './DeliveryReducer';
import SizeSelectionReducer from './SizeSelectionReducer';
import PaymentReducer from './PaymentReducer';

const rootReducer = combineReducers({
  sizeselection:SizeSelectionReducer,
  customize:CustomizeReducer,
  delivery:DeliveryReducer,
  payment:PaymentReducer
});

export default rootReducer;
