import React, { Component } from 'react';
import '../App.css';
import '../css/customize.css';

import wifi from '../images/wifi.png';
import capture from '../images/Capture.PNG';

import paintbr from '../images/paintbr.png';
import { Button,ButtonGroup,FormGroup, } from 'reactstrap';
import { Link } from "react-router-dom";
import icon from '../images/thankyou-icon.PNG';
import barcodepink from '../images/barcodepink.png';
import barcode from '../images/barcos.png';
import preview from '../images/preview.png';
import l from '../images/l.jpg';
import long2 from '../images/long2.jpg';
import baby from '../images/baby.jpg';
import long3 from '../images/long3.jpg';
import {connect} from 'react-redux';
import * as actions from '../Actions/customize-actions';
import SubHeaderWizard from './SubHeaderWizard';
import speaker from '../images/speaker.png';
import { ReactMic } from 'react-mic';
import micro from '../images/microphone.png';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import ReactAudioPlayer from 'react-audio-player';
import BackCard  from './BackCard';
import FrontCard from './FrontCard';
import InnerLeftCard from './InnerLeftCard';
import InnerRightCard from './InnerRightCard';

import Calendar from 'react-input-calendar';
var moment = require('moment');
class Customize extends Component {
constructor(props) {
    super(props);

    console.log('date:',this.props.insidecardDate);
  

   this.handleInputChange = this.handleInputChange.bind(this);
   this.handleSubmit=this.handleSubmit.bind(this);
   this.handleImageChange=this.handleImageChange.bind(this);
   this.handleDateChange = this.handleDateChange.bind(this);
   this.onStop=this.onStop.bind(this);
   console.log('recorded value in state:',this.props.recordedFileUrl);
}

getStyle() {
      if(this.props.checked){
          return{
                display : 'block'
          }
    }else{
          return{
            display : 'none'
          }
    }
}
getImageStyle() {
  if((this.props.insidecardPreviewUrl2) ===''){
    return{
      width:'100%',
      height:'348px',
      display:'block',
      position:'relative',
      overflow:'hidden',
      padding:'33.16% 0 0 0'
          
    }
  }
}

handleSubmit(e){

  var frontcardData={frontcardTitle:this.refs.frontcardTitle.value, frontcardSubTitle:this.refs.frontcardSubTitle.value,frontcardDesc:this.refs.frontcardDesc.value,}
    
    if(this.refs.frontcardTitle.value!=='' && this.refs.frontcardSubTitle.value!=='' && this.refs.frontcardDesc.value!=='' ){
        this.props.submitForm(frontcardData);
    }
}

startRecording = () => {
  this.props.startRecording();
}

stopRecording = () => {

  this.props.stopRecording();
}

onData(recordedBlob) {
  console.log('chunk of real-time data is: ', recordedBlob);
}

onStop(recordedBlob) {
 
  console.log('recordedBlob is: ', recordedBlob);
  console.log('recordedBlob is: ', recordedBlob.blobURL);
  this.props.setRecordedFile(recordedBlob.blobURL);
}

handleInputChange(event) {
  const target = event.target;
  const value =  target.value;
  const name = target.name;

  this.props.handleInputChange(name,value);
}

handleDateChange(date) {

  this.props.handleDateChange(date);
  // this.setState({
  //   startDate: date
  // });
}

handleImageChange(e,imageid) {
  e.preventDefault();
  let reader = new FileReader();
  let file = e.target.files[0];


  reader.onloadend = () => {

    this.props.handleImageChange(imageid,file,reader.result);
    console.log(file);
    console.log(reader.result);
  }

    if(file){
      reader.readAsDataURL(file);
    }
 
}

handleSubmit(e){

  var frontcardData={frontcardTitle:this.refs.frontcardTitle.value, frontcardSubTitle:this.refs.frontcardSubTitle.value,frontcardDesc:this.refs.frontcardDesc.value,}
    
    if(this.refs.frontcardTitle.value!=='' && this.refs.frontcardSubTitle.value!=='' && this.refs.frontcardDesc.value!=='' ){
        this.props.submitForm(frontcardData);
    }
}


decrement = () =>{
  if(this.props.price >0){
    this.props.decrement(this.props.incrementValue);
}
}

increment = () =>{
  this.props.increment(this.props.incrementValue);
}


handleCheckbox = () =>{
//console.log(this.props.incrementValue);

let incrementValue, price;
let quantity=this.props.numberOfCards;

let checked= !this.props.checked;
console.log('this.state.checked:',checked);
if(checked){

  incrementValue=15;
  price=quantity*incrementValue;
 
}else{
  incrementValue= 10;
  price=quantity*incrementValue;
}

this.props.handleCheckbox(incrementValue,price);
}

 render() {
   let $frontPreviewimage = null;
   let $middlePreviewimage1 = null;
   let $middlePreviewimage2 = null;
   
     let {frontcardPreviewUrl} = this.props;

     let {insidecardPreviewUrl1} = this.props;

     let {insidecardPreviewUrl2}=this.props;


    if (frontcardPreviewUrl) {
        $frontPreviewimage = (<img className="lower-frontcardimage" src={frontcardPreviewUrl} alt="no"/> );
  }else{
        $frontPreviewimage = (<img className="lower-frontcardimage" src={baby} alt="no" /> );

  }if (insidecardPreviewUrl1) {
        $middlePreviewimage1=(<img className="lower-middlecard-image" alt="no" src={insidecardPreviewUrl1} />);
  }else{
        $middlePreviewimage1=(<img className="lower-middlecard-image" alt="no" src={l} />);

  } if(insidecardPreviewUrl2){
        $middlePreviewimage2=(<img className=" lower-middlecard-image " alt="no" src={insidecardPreviewUrl2} />);

    }else {
       //$middlePreviewimage2=(<img className=" lower-middlecard-image" alt="no"   src={long} />);

     $middlePreviewimage2=''
      // $imagePreview = (<div className="previewText">Please select an Image htmlFor Preview</div>);

    }

    let currentloc='';
     if(this.props.match.isExact){
         currentloc=this.props.location.pathname;
        //console.log(this.props.match);
     }
    
    
    return (
    
      <div className="App">
        <div className="container content">
        <SubHeaderWizard currentloc={currentloc}/>


              <div className="customize-header">
                <h3> <strong>CUSTOMIZE</strong> </h3>
                <img src={paintbr} alt="no" height='50px' width="50px"/>
              </div>

              <div className="row ">
              <div className="lower-content">
                  <div className="col-lg-2 col-md-12 col-sm-12 card text-center">

                      <h1 className="price">${this.props.price}.00</h1>

                        <div>
                            <ButtonGroup>
                                <Button type="button"  onClick={this.decrement.bind(this)}  className="text-danger counter">-</Button>{' '}
                                <Button className="counter">{this.props.numberOfCards}</Button>{' '}
                                <Button  className="counter" type="Button" onClick={this.increment.bind(this)} >+</Button>{' '}
                            </ButtonGroup>
                        </div>


                        <p className="card1-text">Card Quantity</p>

                        <div>                                
                        <b>Sound Module </b>

                        <label className="switch" id="someSwitchOptionWarning">
                            <input defaultChecked={this.props.checked}  onClick={this.handleCheckbox.bind(this)} name="someSwitchOption001" type="checkbox" />
                            <span className="slider"></span>
                        </label>
                        </div> 

                  </div>
                  <div className="col-lg-10 col-md-12 col-sm-12">
                  <div className="row ">
                  <div className="col-lg-3 col-md-6 col-sm-6 front-card">
                  
                        <p className="title">FRONT OF CARD</p>
                          <form onSubmit={this.handleSubmit.bind(this)}>

                              <div className="form-group">
                                <input type="file"   onChange={(e)=>this.handleImageChange(e,1)} accept="image/*" imgextension={['.jpg', '.gif', '.png', '.gif']} maxfilesize={5242880} className="form-control-file frontcardFile" ref="frondcardimage" />
                              </div>

                              <div className="form-group">
                                <input maxLength="7" onChange={this.handleInputChange}  name="frontcardTitle" ref="frontcardTitle" type="text" className="form-control title text-center" placeholder="Title"   />
                              </div>
                              
                              <div className="form-group">
                                  <input onChange={this.handleInputChange}  name="frontcardSubTitle"  ref="frontcardSubTitle" type="text" className="form-control subTitle text-center" placeholder="Subtitle" />
                              </div>
                             
                              <textarea onChange={this.handleInputChange} name="frontcardDesc" ref="frontcardDesc" type="text" className="form-control text-center" placeholder="Description" cols="30" rows="2"  >
                              </textarea>
                          </form>
                 
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-6 inside-card" >
              
                  <p className="title">INSIDE OF CARD</p>
                        <form>
                        <div className="form-group">
                          <input type="file" accept="image/*" imgextension={['.jpg', '.gif', '.png', '.gif']} maxfilesize={5242880}
                                onChange={(e)=>this.handleImageChange(e,2)}  ref="insidecardimage1" />

                        </div>
                        <div className="form-group">
                          <input type="file" accept="image/*" className="form-control-file" accept="image/*" imgextension={['.jpg', '.gif', '.png', '.gif']}
                             onChange={(e)=>this.handleImageChange(e,3)}  maxfilesize={5242880}  ref="insidecardimage2"/>
                        </div>
                            <div className="form-group">
                              <input maxLength="20" onChange={this.handleInputChange}   name="insidecardTitle"  ref="insidecardTitle" type="text" className="form-control text-center" placeholder="Title"/>
                            </div>
                           
                            <div className="form-group">
                              <textarea onChange={this.handleInputChange} name="insidecardDesc"  ref="insidecardDesc" type="text" className="form-control text-center" cols="30" rows="2" placeholder="Decription"  >
                              </textarea>
                          </div>
                            <div className="form-group">
                                  <input onChange={this.handleInputChange} ref="insidecardSignature" type="text" className="form-control text-center" name="insidecardSignature" placeholder="Thankyou/signature" />
                            </div>


                           

                           {/* <div className="form-group">
                              {/* <i className="fa fa-microphone record-icon" ></i> 
                              <DatePicker
                                className="date-picker"
                                selected={this.props.insidecardDate}
                                onChange={this.handleDateChange}
                              />
                              </div> */}
                              
                            <div className=" form-control input-group">
                            <div className="input-group-addon"><i className="glyphicon glyphicon-calendar" ></i></div>
                                <input onChange={this.handleInputChange} name="insidecardDate"  ref="insidecardDate" type="date" className="insidecard-date"/>
                            </div>
  
                            <div className="form-group">
                                  
                            </div>
                        </form>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-6 sound-card" style = {this.getStyle()}>
                
                  <p className="title">SOUND MODULE</p>
                        <form>
                              <img className="upper-soundcard-images" src={speaker} alt="not" height='50px' width="50px"/>
                              
                            <div className="form-group " >
                           
                              <input type="file" accept="audio/*" className="soundcard-input"  name="uploadFile" id="uploadFile"/>
                            </div>
                            <img className="upper-soundcard-images" src={micro} alt="no" height='50px' width="50px"/>

                            <div className="soundcard-mic ">
                            <ReactMic
                              record={this.props.record}
                              className="sound-wave"
                              onStop={this.onStop}
                              strokeColor="#000000" 
                              backgroundColor="#FF7B7B" />
                            
                            <div className="sound-player text-center">
                            <ReactAudioPlayer
                             
                              src={this.props.recordedFileUrl}
                              //autoPlay
                              controls
                            />

                            </div>
                            {this.props.record ?
                              <button onClick={this.stopRecording} className="btn btn-circle btn-lg stop-record" type="button"><i className="fa fa-stop record-icon" ></i></button>  
                              :
                              <button onClick={this.startRecording} className="btn btn-circle btn-lg start-record" type="button"><i className="fa fa-microphone record-icon" ></i></button>

                            }
                          </div>
                        </form>
                        
                 
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-6 back-card">
                   
                  <p className="title">BACK OF CARD</p>
                          <form>
                          <img className="back-card-image" src={barcode} alt="no" height='50px' width="50px"/>
                          {/* <span className="backcard-or"> or </span>
                            <img src={wifi} alt="no" height='50px' width="50px"/> */}

                              <div className="form-group">
                                <input type="text" className="form-control text-center" placeholder="Link Address" />
                              </div>
                              <div className="form-group">
                                  <input type="text" className="form-control text-center" placeholder="Subtitle" />
                              </div>

                              <div className="form-group">
                                  <input type="text" className="form-control text-center" placeholder="Thankyou/signature" />
                              </div>

                   </form>


                  </div>
                  </div>
                  </div>
                  </div>
                </div>

                <hr />

              <div className="customize-header">
                <div className="row center-block">

                        <p className="btn btn-lg" onClick={this.handleSubmit.bind(this)}><strong>PREVIEW</strong></p>
                        </div>

                  <div className="row">

                            <img src={preview} alt="no" height='50px' width="50px"/>

                    </div>

            </div>



            <div className="row ">
                
                <div className="lower-content">
                 <div className="col-lg-2 col-md-12 col-sm-12 lower-frontcard ">
                       <FrontCard 
                          frontcardTitle={this.props.frontcardTitle}
                          frontcardSubTitle={this.props.frontcardSubTitle}
                          frontcardDesc={this.props.frontcardDesc}
                          frontPreviewimage={$frontPreviewimage}
                       />
                  </div>


                  <div className="col-lg-5 col-md-12 col-sm-12 lower-middlecard">
                 
                      <div className="row">

                              <div className="col-lg-6 col-md-6 col-sm-6 lower-middlecard-left">
                                
                                      <InnerLeftCard 
                                      getImageStyle={this.getImageStyle()}
                                      middlePreviewimage1={$middlePreviewimage1} 
                                      middlePreviewimage2={$middlePreviewimage2}

                                         />
                                </div>

                          <div className="col-lg-6 col-md-6 col-sm-6 ">

                                    <InnerRightCard
                                      insidecardTitle={this.props.insidecardTitle}
                                      insidecardDate={this.props.insidecardDate}
                                      insidecardDesc={this.props.insidecardDesc}
                                      insidecardSignature={this.props.insidecardSignature}
                                      
                                      />
                                 
                                  

                          </div>

                      </div>
                    
                  </div>

                  <div className="col-lg-2 col-md-12 col-sm-12 lower-backcard">
                  <div className="inputcard" >
                      
                   <BackCard />

                   

                  </div>
                  </div>
                          </div>
                </div>


            

                <div className="row">
                <div className="col-md-6">
                        <div className="pull-right">
                        <Link className="btn btn-warning contentButton" to="/SizeSelection">Go Back</Link>


                        </div>
                </div>
                <div className="col-md-6 ">
                        <div className="pull-left">
                        <Link className="btn btn-warning contentButton" to="/Delivery"> Proceed to Delivery</Link>

                        </div>
                    </div>
                </div>
          </div>
      </div>
    );
  }
}

function mapStateToProps(state){
  let customize=state.customize;
  return {
    price:customize.price,
    numberOfCards:customize.numberOfCards,
    incrementValue:customize.incrementValue,
    checked:customize.checked,
    record:customize.record,
    recordedFileUrl:customize.recordedFileUrl,
    frontcardTitle:customize.frontcardTitle,
    frontcardSubTitle:customize.frontcardSubTitle,
    frontcardDesc:customize.frontcardDesc,
    insidecardSignature:customize.insidecardSignature,
    insidecardTitle:customize.insidecardTitle,
    insidecardDesc:customize.insidecardDesc,
    insidecardDate:customize.insidecardDate,

    frontcardFile:customize.frontcardFile,
    frontcardPreviewUrl:customize.frontcardPreviewUrl,

    insidecardFile1:customize.insidecardFile1,
    insidecardPreviewUrl1:customize.insidecardPreviewUrl1,

    insidecardFile2:customize.insidecardFile2,
    insidecardPreviewUrl2:customize.insidecardPreviewUrl2,
    
  }
}
export default connect(mapStateToProps,actions)(Customize);

