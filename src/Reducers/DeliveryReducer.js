import { SUBMITDELIVERYFORM  ,DELIVERYINPUTCHANGE} from '../Actions/actions-type';

const initialState = {
    firstName:'',
    lastName:'',
    address: '',
    city:'',
    unit:'',
    email:'',
    province:'',
    postalCode: '',
    country:''
};

export default function (state=initialState,action){

switch(action.type){
        case DELIVERYINPUTCHANGE:
            return {
                ...state,
               [action.name] : action.value
            };
            break;

        case SUBMITDELIVERYFORM:
            return{
                    ...state,
                    firstName:action.data.firstName,
                    lastName:action.data.lastName,
                    address: action.data.address,
                    city:action.data.city,
                    unit:action.data.unit,
                    province:action.data.province,
                    postalCode: action.data.postalCode,
                    country:action.data.country,                        
                    email:action.data.email,
                }
                break;
       
            default:
                return state;
        }
}