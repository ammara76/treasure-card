import {UPDATESIZE} from './actions-type';


export function updateSize(mysize){
    return{
        type: UPDATESIZE,
        payload:mysize
    }
};

