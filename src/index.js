import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import reducers from './Reducers/';
import { Provider } from 'react-redux';
import {StripeProvider} from 'react-stripe-elements';
import { createStore, applyMiddleware } from 'redux';
//import 'bootstrap/dist/css/bootstrap.min.css';


const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
<Provider store={createStoreWithMiddleware(reducers)}>
     <StripeProvider apiKey="pk_test_DfeY2XIWkIKC9MkzeLjjzx24">
        <App />
    </StripeProvider> 
</Provider>,
 document.getElementById('root'));
registerServiceWorker();
