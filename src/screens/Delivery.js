import React, { Component } from 'react';
import '../App.css';
import '../css/delivery.css';
import envelop from '../images/envelop.png';
import { Link } from "react-router-dom";
import {connect} from 'react-redux';
import * as actions from '../Actions/delivery-actions';
import SubHeaderWizard from './SubHeaderWizard';
import { Form, FormGroup, Label,Button, Input, FormFeedback, FormText,Row,Col } from 'reactstrap';
import { ValidatingFormGroup } from 'reactstrap-validation';
import { AvForm, AvField } from 'availity-reactstrap-validation';

class Delivery extends Component {
    constructor(props) {
        super(props);
        
      
       this.handleInputChange = this.handleInputChange.bind(this);
       this.submitDeliveryForm=this.submitDeliveryForm.bind(this);

       this.handleValidSubmit = this.handleValidSubmit.bind(this);
       this.handleInvalidSubmit = this.handleInvalidSubmit.bind(this);
}


handleInputChange(event) {
    const target = event.target;
    const value =  target.value;
    const name = target.name;

    this.props.handleInputChange(name,value);

  }

submitDeliveryForm(e){

    console.log('submitted');
    e.preventDefault();
  var data={firstName:this.refs.firstName.value, lastName:this.refs.lastName.value,email:this.refs.email.value ,
     address:this.refs.address.value ,
    city:this.refs.city.value, province:this.refs.province.value, unit:this.refs.unit.value,
    postalCode:this.refs.postalCode.value, country:this.refs.country.value}
    

    this.props.submitDeliveryForm(data);
    this.props.history.push("/Payment");

    // if(this.refs.firstName.value!=='' && this.refs.lastName.value!=='' ){
    //    
        
    //     //this.router.push('/Payment');
    //     
    

    // }else{
    //     alert('fill required fields');
    // }
}

handleValidSubmit(event, values) {
    console.log('submitted');
    //this.setState({email: values.email});
  }

  handleInvalidSubmit(event, errors, values) {
    console.log('not submitted successfully');

    //this.setState({ error: true});
  }

 render() {

    let currentloc='';
     if(this.props.match.isExact){
         currentloc=this.props.location.pathname;
        //console.log(this.props.match);
     }
    
     console.log('props:',this.props.firstName,this.props.lastName);

    return (
    
      <div className="App">
        <div className="container content">
        <SubHeaderWizard currentloc={currentloc}/>

                <div className="customize-header">
                  <h3> <strong>DELIVERY</strong> </h3>
                  <img src={envelop} alt="no" height='70px' width="70px"/>
                </div>
                    
                <AvForm className="form-inline" onValidSubmit={this.submitDeliveryForm} onInvalidSubmit={this.handleInvalidSubmit}>
                  {/* <form onSubmit={this.submitDeliveryForm.bind(this)}> */}

                     <div className="row form-row">
                        <div className="col-lg-4">
                          <div className="form-group">
                            <div className="row">
                                <label htmlFor="First col-md-2">First Name<span className="required-star  text-danger">*</span></label>
                                <AvField  type="text col-md-2 " className="form-control" onChange={this.handleInputChange} value={this.props.firstName}  name="firstName" ref="firstName" required />
                          </div>
                          </div>
                        </div>

                        <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                              <label  htmlFor="Last">Last Name<span className="required-star text-danger">*</span></label>
                                <AvField type="text col-md-2 " className="form-control" onChange={this.handleInputChange} value={this.props.lastName} name="lastName" ref="lastName" required />
                              {/* <input  required   value={this.props.lastName} name="lastName" ref="lastName" type="text col-md-2" className="form-control"/> */}
                          </div>
                          </div>
                        </div>
                    </div>


                    <div className="row form-row">
                        <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                              <label htmlFor="email">Email<span className="required-star text-danger">*</span></label>
                              <AvField onChange={this.handleInputChange}  value={this.props.email} ref="email" name="email" type="email" className="form-control" required/>
                       
                          </div>
                          </div> 
                        </div>

                      <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                              <label htmlFor="City">City</label>       
                              <AvField onChange={this.handleInputChange}  value={this.props.city} name="city" ref="city" type="text" className="form-control" />

                           
                          </div>
                          </div>
                        </div>
                    </div>


                    <div className="row form-row">
                      <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                              <label htmlFor="Unit">Unit#</label>
                              <AvField onChange={this.handleInputChange}  value={this.props.unit} name="unit" ref="unit" type="text" className="form-control" />

                              </div>
                          </div>
                      </div>

                      <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                              <label htmlFor="Province"> Province</label>
                              <AvField onChange={this.handleInputChange}  value={this.props.province} name="province" ref="province" type="text" className="form-control" />

                             
                          </div>
                          </div>
                      </div>
                    </div>

                    <div className="row form-row">
                      <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                              <label htmlFor="Postal">Postal Code#</label>
                              {/* <span className="text-danger">  </span> */}
                              <AvField  onChange={this.handleInputChange}  value={this.props.postalCode} name="postalCode" ref="postalCode" type="text" className="form-control"/>
                          </div>
                          </div>
                      </div>

                      <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                              <label htmlFor="Country">Country<span className="required-star text-danger">*</span> </label>
                                                           
                             <AvField required onChange={this.handleInputChange}  value={this.props.country} name="country" ref="country" type="text" className="form-control"/>
                          </div>
                          </div>
                      </div>
                    </div> 

                    <div className="row form-row">
                        <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                              <label htmlFor="Address">Address</label>
                              <AvField  onChange={this.handleInputChange}  value={this.props.address} name="address" ref="address" type="text " className="form-control"/>
                          </div>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group">
                          <div className="row">
                          </div>
                          </div>
                        </div>
                      
                    </div>

                           {/* <Button color="primary">Submit</Button> */}
                     {/* </AvForm> */}

               
            

                 <div className="row">
                <div className="col-md-6">
                        <div className="pull-right">
                        <Link className="btn btn-warning contentButton" to="/Customize">Go Back</Link>

                        </div>
                </div>
                <div className="col-md-6 ">
                        <div className="pull-left">
                            <Button className="btn btn-warning contentButton"> Submit Delivery Form</Button>

                        </div>
                    </div>
                </div>
                </AvForm>
                 
             
                 

          </div>
        </div>

    );
  }
}

function mapStateToProps(state){

    let delivery=state.delivery;
    return {
        firstName:delivery.firstName,
        lastName:delivery.lastName,
        email:delivery.email,
        address: delivery.address,
        city:delivery.city,
        unit:delivery.unit,
        province:delivery.province,
        postalCode: delivery.postalCode,
        country:delivery.country
      
    }
  }
  export default connect(mapStateToProps,actions)(Delivery);


//   import React, { Component } from 'react';
// import '../App.css';
// import '../css/delivery.css';
// import envelop from '../images/envelop.png';
// import { Link } from "react-router-dom";
// import {connect} from 'react-redux';
// import * as actions from '../Actions/delivery-actions';
// import SubHeaderWizard from './SubHeaderWizard';
// import { Form, FormGroup, Label,Button, Input, FormFeedback, FormText,Row,Col } from 'reactstrap';
// import { ValidatingFormGroup } from 'reactstrap-validation';
// import { AvForm, AvField } from 'availity-reactstrap-validation';

// class Delivery extends Component {
//     constructor(props) {
//         super(props);
    
//        this.handleInputChange = this.handleInputChange.bind(this);
//        this.submitDeliveryForm=this.submitDeliveryForm.bind(this);
// }


// handleInputChange(event) {
//     const target = event.target;
//     const value =  target.value;
//     const name = target.name;

//     this.props.handleInputChange(name,value);

//   }

// submitDeliveryForm(e){
//     e.preventDefault();
//   var data={firstName:this.refs.firstName.value, lastName:this.refs.lastName.value,email:this.refs.email.value ,
//      address:this.refs.address.value ,
//     city:this.refs.city.value, province:this.refs.province.value, unit:this.refs.unit.value,
//     postalCode:this.refs.postalCode.value, country:this.refs.country.value}
    
//     if(this.refs.firstName.value!=='' && this.refs.lastName.value!=='' ){
//         this.props.submitDeliveryForm(data);
        
//         //this.router.push('/Payment');
//         this.props.history.push("/Payment");
    

//     }else{
//         alert('fill required fields');
//     }
// }

//  render() {

//     let currentloc='';
//      if(this.props.match.isExact){
//          currentloc=this.props.location.pathname;
//         //console.log(this.props.match);
//      }
    
    
//     return (
    
//       <div className="App">
//         <div className="container content">
//         <SubHeaderWizard currentloc={currentloc}/>

//                 <div className="customize-header">
//                   <h3> <strong>DELIVERY</strong> </h3>
//                   <img src={envelop} alt="no" height='70px' width="70px"/>
//                 </div>
                    
//                   <form className="form-inline" onSubmit={this.submitDeliveryForm.bind(this)}>

//                      <div className="row form-row">
//                         <div className="col-lg-4">
//                           <div className="form-group">
//                             <div className="row">
                            
//                               <label htmlFor="First col-md-2">First Name<span className="required-star  text-danger">*</span></label>
                              
//                               <input required onChange={this.handleInputChange}  value={this.props.firstName}  name="firstName"
//                                     ref="firstName" type="text col-md-2" className="form-control"/>
                           
                           
//                           </div>
//                           </div>
//                         </div>

//                         <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                               <label  htmlFor="Last col-md-2">Last Name<span className="required-star text-danger">*</span></label>
                              
//                               <input  required onChange={this.handleInputChange}  value={this.props.lastName} name="lastName" ref="lastName" type="text col-md-2" className="form-control"/>
//                           </div>
//                           </div>
//                         </div>
//                     </div>


//                     <div className="row form-row">
//                         <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                               <label htmlFor="email">Email<span className="required-star text-danger">*</span></label>
                                                           
//                               <input required  onChange={this.handleInputChange}  value={this.props.email} name="email" ref="email" type="text " className="form-control" required/>
//                           </div>
//                           </div>
//                         </div>

//                       <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                               <label htmlFor="City">City</label>       
//                               {/* <span className="text-danger">  </span>      */}
//                             <input  onChange={this.handleInputChange}  value={this.props.city} name="city" ref="city" type="text" className="form-control"/>
//                           </div>
//                           </div>
//                         </div>
//                     </div>


//                     <div className="row form-row">
//                       <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                               <label htmlFor="Unit">Unit#</label>
//                               {/* <span className="text-danger">  </span> */}
//                               <input  onChange={this.handleInputChange}  value={this.props.unit} name="unit" ref="unit" type="text" className="form-control"/>
//                               </div>
//                           </div>
//                       </div>

//                       <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                               <label htmlFor="Province"> Province</label>
//                               {/* <span className="text-danger">  </span> */}
//                              <input  onChange={this.handleInputChange}  value={this.props.province} name="province" ref="province" type="text" className="form-control"/>
//                           </div>
//                           </div>
//                       </div>
//                     </div>

//                     <div className="row form-row">
//                       <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                               <label htmlFor="Postal">Postal Code#</label>
//                               {/* <span className="text-danger">  </span> */}
//                               <input  onChange={this.handleInputChange}  value={this.props.postalCode} name="postalCode" ref="postalCode" type="text" className="form-control"/>
//                           </div>
//                           </div>
//                       </div>

//                       <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                               <label htmlFor="Country">Country<span className="required-star text-danger">*</span> </label>
                                                           
//                              <input  onChange={this.handleInputChange}  value={this.props.country} name="country" ref="country" type="text" className="form-control"/>
//                           </div>
//                           </div>
//                       </div>
//                     </div> 

//                     <div className="row form-row">
//                         <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                               <label htmlFor="Address">Address</label>
//                               {/* <span className="text-danger">  </span> */}
                              
//                               <input  onChange={this.handleInputChange}  value={this.props.address} name="address" ref="address" type="text " className="form-control" required/>
//                           </div>
//                           </div>
//                         </div>
//                         <div className="col-lg-4">
//                           <div className="form-group">
//                           <div className="row">
//                           </div>
//                           </div>
//                         </div>
                      
//                     </div>

//                          {/* <div className="form-button">
//                             <button   onClick={this.submitDeliveryForm.bind(this)} type="submit"  className="center-block btn btn-secondary" >Submit</button>
//                         </div> */}
//                     </form>

               
            

//                  <div className="row">
//                 <div className="col-md-6">
//                         <div className="pull-right">
//                         <Link className="btn btn-warning contentButton" to="/Customize">Go Back</Link>

//                         </div>
//                 </div>
//                 <div className="col-md-6 ">
//                         <div className="pull-left">
//                              <a onClick={this.submitDeliveryForm.bind(this)} className="btn btn-warning contentButton"> Submit Delivery Form</a>

//                         </div>
//                     </div>
//                 </div>
                 
             
//                     {/* <Form>
//                                 <ValidatingFormGroup trigger="change" valid={true} onStateChange={console.log}>
//                                     <Input required type="email" />
//                                     <FormFeedback>Please Enter the required field!</FormFeedback>
//                                 </ValidatingFormGroup>
//                             </Form>

//                             <Form>
//                                 <ValidatingFormGroup trigger="change" valid>
//                                 <Label for="exampleEmail">Valid input</Label>
//                                 <Input valid />
//                                 <FormFeedback valid>Sweet! that name is available</FormFeedback>
//                                 <FormText>Example help text that remains unchanged.</FormText>
//                                 </ValidatingFormGroup>
//                                 <FormGroup>
//                                 <Label for="examplePassword">Invalid input</Label>
//                                 <Input invalid />
//                                 <FormFeedback>Oh noes! that name is already taken</FormFeedback>
//                                 <FormText>Example help text that remains unchanged.</FormText>
//                                 </FormGroup>
//                         </Form> */}

//           </div>
//         </div>

//     );
//   }
// }

// function mapStateToProps(state){

//     let delivery=state.delivery;
//     return {
//         firstName:delivery.firstName,
//         lastName:delivery.lastName,
//         email:delivery.email,
//         address: delivery.address,
//         city:delivery.city,
//         unit:delivery.unit,
//         province:delivery.province,
//         postalCode: delivery.postalCode,
//         country:delivery.country
      
//     }
//   }
//   export default connect(mapStateToProps,actions)(Delivery);