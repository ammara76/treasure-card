import React, { Component } from 'react';
import { Nav, NavItem  } from 'reactstrap';
import { Link } from "react-router-dom";
import { NavLink } from 'react-router-dom'


import '../App.css';


class Header extends Component {




  render() {

    return (
       
      <div className="App">
        <div className="container ">
          
            <div>
            <h1 className="text-center header-title"> TreasureCard</h1>
            </div>
            <div >

            <Nav tabs className="navbar " role="navigation">
              <div className="nav navbar-nav" >
                <NavItem>
                  <Link className="nav-items active" to="/">Home</Link>
                </NavItem>
                <NavItem>
                <NavLink className="nav-items" activeClassName="selected" to="/faq">FAQ</NavLink>
          
                </NavItem>
                <NavItem>
                <Link className="nav-items" to="/media">Media</Link>

                </NavItem>
                <NavItem>
                <Link className="nav-items" to="/SignUp">Sign Up</Link>

                </NavItem>
                </div>
          </Nav>
          </div>

</div>
</div>
    );
  }
}

export default Header;
