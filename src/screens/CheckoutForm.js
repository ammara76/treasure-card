import React, { Component } from 'react';
import {injectStripe} from 'react-stripe-elements';
import '../css/checkoutform.css';
import {connect} from 'react-redux';
import * as actions from '../Actions/payment-actions';
import {StripeProvider} from 'react-stripe-elements';
import {CardElement} from 'react-stripe-elements';
import {PaymentRequestButtonElement} from 'react-stripe-elements';
const $displayError = '';

const stripeTokenHandler = (token) => {

    // Insert the token ID into the form so it gets submitted to the server
    const form = document.getElementById('payment-form');
    const hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);
  
    // Submit the form
    form.submit();
  }

class CheckoutForm extends Component {   

    constructor(props){
        super(props);

        this.state={
            token:''
        };
    }

addEventListener=(event)=>{
    let  $displayError = '';
    if (event.error) {
        console.log(event.error);
        $displayError= event.error.message;
    } else {
        
        $displayError = '';
    }
}

    handleSubmit =(ev)=>{

        ev.preventDefault();


        let fName=this.props.firstName;
        let lName=this.props.lastName;

        if(!fName){
            fName='Ammara';
        }

        if(!lName){
            lName='Sana';
        }

        let  $displayError = '';

        //let this2 = this;
            this.props.stripe.createToken({name:fName+' '+lName,}).then(function(result) {
            if (result.error) {
              
                console.log(result.error);
            
              $displayError= 'errroorr';
            } else {
                
                console.log(result.token);

                // this2.setState({ paymentComplete: true, submitDisabled: false, token: result.id });
                // // make request to your server here!
                // const paymentRequest = props.stripe.paymentRequest({
                //     country: 'US',
                //     currency: 'usd',
                //     total: {
                //       label: 'Demo total',
                //       amount: 1000,
                //     },
                //   });

                //   paymentRequest.on('token', ({complete, token, ...data}) => {
                //     console.log('Received Stripe token: ', token);
                //     console.log('Received customer information: ', data);
                //     complete('success');
                //   });
                
               
        // const paymentRequest = this.props.stripe.paymentRequest({
        //     country: 'US',
        //     currency: 'usd',
        //     total: {
        //       label: 'Demo total',
        //       amount: 1000,
        //     },
        //   });

        //   this.setState({
        //       paymentRequest:paymentRequest
        //   })


        //     const elements = this.props.stripe.elements();
        //     const prButton = elements.create('paymentRequestButton', {paymentRequest,});
                    // (async () => {
                    // // Check the availability of the Payment Request API first.
                    // const nextResult = await paymentRequest.canMakePayment();
                    // console.log('rs:',nextResult);
                    // if (nextResult) {

                    //     this.setState({
                    //         canMakePayment:true,
                    //     });

        
                    //     // console.log('if cond');
                    //     // prButton.mount('#payment-request-button');
                    // } else {
                    //     console.log('else:');
                    //     document.getElementById('payment-request-button').style.display = 'none';
                    // }
                    // })();
            
              // Send the token to your server.
              //stripeTokenHandler(result.token);
    
              
            }
          });
    }
    render() {

        return (
        <div className="App">
            <div className="Checkout">
                 <form onSubmit={this.handleSubmit} id="payment-form">
                      <div className="form">
                      <label className="card-header" htmlFor="card-element">
                         <h3> Credit or debit card  </h3>
                      </label>

                      <div ref="card-element" id="card-element">
                          <CardElement onChange={this.addEventListener} style={{base:{fontSize :'18px'}}} />
                      </div>

                      <div id="card-errors" role="alert"></div>
                       
                      </div>

                      <button className="payment-button">Submit Payment</button>
                  </form>
            </div>

           
        </div>
    
        );
      }
}

function mapStateToProps(state){

    
    let payment=state.payment;
    return {
        
        firstName:payment.firstName,
        lastName:payment.lastName,
        cardNumber: payment.cardNumber,
        cardExpiry:payment.cardExpiry,
        cardCVC:payment.cardCVC,
        couponCode:payment.couponCode
    }
  }
  
export default injectStripe(connect(mapStateToProps,actions)(CheckoutForm));
// export default connect(mapStateToProps, mapDispatchToProps, mergeProps, actions ,{
//     pure: false,
//   })(injectStripe(CheckoutForm));
