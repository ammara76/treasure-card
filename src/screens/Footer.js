import React, { Component } from 'react';

import '../App.css';


class Footer extends Component {


  render() {
    return (
      <div className="App">
        <div className="footer">
            <p>&copy; 2017-2018 Treasure Card Inc. &middot; Privacy &middot; Terms</p>
          </div>
      </div>
    );
  }
}

export default Footer;
