import React, { Component } from 'react';
import { Nav, NavItem } from 'reactstrap';
import { Link } from "react-router-dom";
import { NavLink } from 'react-router-dom';
import '../App.css';


class SubHeaderWizard extends Component {
    constructor(props) {
        super(props);
    
       this.state={
        SizeSelectionActive:'',
        PaymentActive:'',
        DeliveryActive:'',
        CustomizeActive:'',
        ThankyouActive:''
        
       };


}

componentWillMount(){
    
    console.log(this.props.currentloc) ;
    let currentpage=this.props.currentloc;
    
    if(currentpage.includes('/SizeSelection')){
      
        this.setState({
            SizeSelectionActive:'active'
        });
      
    }else if(currentpage.includes('/Customize')){
      
        this.setState({
            CustomizeActive:'active'
        });
      
    }
    else if(currentpage.includes('/Delivery')){
      
        this.setState({
            DeliveryActive:'active'
        });
      
    }
    else if(currentpage.includes('/Payment')){
      
        this.setState({
            PaymentActive:'active'
        });
    
    }else if(currentpage.includes('/Thankyou')){
      
        this.setState({
        ThankyouActive:'active'
        });
      
    }else{

    }
        
    }




  render() {
    

    return (
      <div className="App">
        <div className="container">
        
          <div className="wizard">
            <div className="wizard-inner">
                <div className="connecting-line"></div>
                <Nav className="nav nav-tabs" role="tablist">

               
                <NavItem  className={this.state.SizeSelectionActive}>
                    <Link to="/SizeSelection" aria-controls="step1" role="tab" title="Step 1">
                        <span className="round-tab">
                          <i className="glyphicon glyphicon-home" ></i>

                        </span>
                    </Link>
                </NavItem>

                    <NavItem  className={this.state.CustomizeActive}>
                        <NavLink   to="/Customize"  aria-controls="step1"  role="tab" title="Step 1">
                            <span className="round-tab">
                              <i className="glyphicon glyphicon-folder-open" ></i>

                            </span>
                        </NavLink>
                    </NavItem>

                    <NavItem  className={this.state.DeliveryActive} >
                        <Link   to="/Delivery"  aria-controls="step2" role="tab" title="Step 2">
                            <span className="round-tab">
                            <i className="glyphicon glyphicon-pencil" ></i>

                            </span>
                        </Link>
                    </NavItem>
                    <NavItem className={this.state.PaymentActive}>
                        <Link to="/Payment" href="#step3"  aria-controls="step3" role="tab" title="Step 3">
                            <span className="round-tab">
                            <i className="glyphicon glyphicon-picture"></i>
                            </span>
                        </Link>
                    </NavItem>

                    <NavItem className={this.state.ThankyouActive}>
                        <Link to="/Thankyou" aria-controls="complete" role="tab" title="Complete">
                          <span className="round-tab">

                                <i className="glyphicon glyphicon-ok" ></i>

                            </span>
                        </Link>
                    </NavItem>
                </Nav>
            </div>


        </div>

</div>
</div>
    );
  }
}

export default SubHeaderWizard;
