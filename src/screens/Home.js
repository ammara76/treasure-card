import React, { Component } from 'react';
import contentimage from '../images/whitecard.jpg';
import '../App.css';
import { Link } from "react-router-dom";

class Home extends Component {

  render() {
    
    return (
    
      <div className="App">
        <div className="container content">
       
              <div >
                  <img className="content-image" src={contentimage} alt="no" width="100" height="50" />
              </div>

              <div className="row text-center">

              <Link className="btn btn-warning contentButton" to="/SizeSelection">Start</Link>
            
              </div>
          </div>
      </div>
    );
  }
}

export default Home;
