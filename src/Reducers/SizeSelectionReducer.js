import {UPDATESIZE} from '../Actions/actions-type';

const initialState = {
    size: "5x7"
};

export default function (state=initialState,action){
    
    switch(action.type){
        case UPDATESIZE:
             
            return {
                    ...state,
                    size: action.payload
                    
                }
                break
          

            default: 
                return state;       
    } 
}